package com.nimbusds.json;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface JSONParser {

    Map<String, Object> newJsonObject();

    List<Object> newJSONArray();

    Map<String, Object> parse(final String s) throws ParseException;

    String toJSONString(Map<String, ?> map);

    String toJSONString(String s);
}

