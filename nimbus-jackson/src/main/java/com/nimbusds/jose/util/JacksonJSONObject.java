package com.nimbusds.jose.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

class JacksonJSONObject extends HashMap<String, Object> {

    private final ObjectMapper mapper;

    JacksonJSONObject(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public String toString() {
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
