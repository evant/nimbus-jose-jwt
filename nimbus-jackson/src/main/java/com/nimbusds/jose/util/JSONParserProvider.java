package com.nimbusds.jose.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.io.JsonStringEncoder;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.json.JSONParser;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

class JSONParserProvider {

    private static JSONParser INSTANCE = new JSONParser() {

        private final ObjectMapper mapper = new ObjectMapper();

        @Override
        public Map<String, Object> newJsonObject() {
            return new JacksonJSONObject(mapper);
        }

        @Override
        public List<Object> newJSONArray() {
            return new JacksonJSONArray(mapper);
        }

        @Override
        public Map<String, Object> parse(String s) throws ParseException {
            try {
                return mapper.readValue(s, new TypeReference<Map<String, Object>>() {
                });
            } catch (JsonParseException e) {
                throw new ParseException("Invalid JSON: " + e.getMessage(), 0);
            } catch (JsonMappingException e) {
                throw new ParseException("JSON entity is not an object", 0);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public String toJSONString(Map<String, ?> map) {
            try {
                return mapper.writeValueAsString(map);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public String toJSONString(String s) {
            return "\"" + new String(new JsonStringEncoder().quoteAsString(s)) + "\"";
        }
    };

    public static JSONParser getInstance() {
        return INSTANCE;
    }
}
