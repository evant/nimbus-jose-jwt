package com.nimbusds.jose.util;

import com.nimbusds.json.JSONParser;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import static net.minidev.json.parser.JSONParser.ACCEPT_TAILLING_SPACE;
import static net.minidev.json.parser.JSONParser.USE_HI_PRECISION_FLOAT;

class JSONParserProvider {

    private static final JSONParser INSTANCE = new JSONParser() {

        @Override
        public Map<String, Object> newJsonObject() {
            return new JSONObject();
        }

        @Override
        public List<Object> newJSONArray() {
            return new JSONArray();
        }

        @Override
        public Map<String, Object> parse(String s) throws ParseException {
            Object o;

            try {
                o = new net.minidev.json.parser.JSONParser(USE_HI_PRECISION_FLOAT | ACCEPT_TAILLING_SPACE).parse(s);

            } catch (net.minidev.json.parser.ParseException e) {

                throw new ParseException("Invalid JSON: " + e.getMessage(), 0);
            }

            if (o instanceof JSONObject) {
                return (Map<String, Object>) o;
            } else {
                throw new ParseException("JSON entity is not an object", 0);
            }
        }

        @Override
        public String toJSONString(Map<String, ?> map) {
            return JSONObject.toJSONString(map);
        }

        @Override
        public String toJSONString(String s) {
            return JSONValue.toJSONString(s);
        }
    };

    public static JSONParser getInstance() {
        return INSTANCE;
    }
}
